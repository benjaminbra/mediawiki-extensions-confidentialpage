<?php

use MediaWiki\User\UserIdentity;
use MediaWiki\Revision\RevisionRecord;
use MediaWiki\Storage\EditResult;

global $wgUser;

class ConfidentialPageHooks {

    public static function onLoadExtensionSchemaUpdates( DatabaseUpdater $updater ) {
        $file = __DIR__ . '/sql/confidentialPage.sql';
        $updater->addExtensionTable('confidential_page', $file);
    }

    public static function addToEditPage(EditPage &$editPage, OutputPage &$output ) {
        $lb = \MediaWiki\MediaWikiServices::getInstance()->getDBLoadBalancer();
        $dbr = $lb->getConnectionRef( DB_REPLICA );
        $res = $dbr->select('confidential_page', ['page_id'], 'page_id = '.$output->getWikiPage()->getId());

        $isConfidential = !!$res->fetchRow();

        $editPage->editFormTextAfterWarn .= '<input id="is_confidential" type="checkbox" name="is_confidential" '. ($isConfidential ? 'checked' : '') .'/>' .
            '<label for="is_confidential">Page confidentielle</label>';
    }

    public static function onEditPage_attemptSave( EditPage $editpage ) {
        $pageId = $editpage->getContext()->getWikiPage()->getId();
        $post = $editpage->getContext()->getRequest()->getPostValues();
        $lb = \MediaWiki\MediaWikiServices::getInstance()->getDBLoadBalancer();
        $dbr = $lb->getConnectionRef( DB_MASTER );
        $res = $dbr->select('confidential_page', ['page_id'], 'page_id = '.$pageId);
        $isConfidential = !!$res->fetchRow();
        if(isset($post['is_confidential']) && !$isConfidential){
            $dbr->insert('confidential_page', ['page_id' => $pageId]);
        } else if($isConfidential) {
            $dbr->delete('confidential_page', 'page_id = ' . $pageId);
        }
    }

    public static function onOutputPageBeforeHTML( OutputPage $out, &$text ) {
        $lb = \MediaWiki\MediaWikiServices::getInstance()->getDBLoadBalancer();
        $dbr = $lb->getConnectionRef( DB_REPLICA );
        $res = $dbr->select('confidential_page', ['page_id'], 'page_id = '.$out->getWikiPage()->getId());

        $isConfidential = !!$res->fetchRow();

        if($out->getUser()->isAnon() && $isConfidential){
            $text = '<div class="console">
                        <p>
                            CONSOLE DES SERVICES DE RENSEIGNEMENTS DE L\'ALLIANCE D\'HELEOS<br/>
                            -Server 2-
                        </p>
                        <p>
                        Vous avez tenté d\'accéder à une page confidentielle.<br/>
                        Merci de naviguer uniquement sur les pages autorisés à votre niveau d\'accréditation.
                        </p>
                    </div>';
        }
    }

    public static function onArticleDeleteAfterSuccess( Title $title, OutputPage $output ) {
        $lb = \MediaWiki\MediaWikiServices::getInstance()->getDBLoadBalancer();
        $dbr = $lb->getConnectionRef( DB_MASTER );
        $dbr->delete('confidential_page', 'page_id = ' . $output->getWikiPage()->getId());
    }

}