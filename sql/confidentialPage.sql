CREATE TABLE /*_*/confidential_page (
    page_id int(10) UNSIGNED unique,
    FOREIGN KEY (page_id) REFERENCES page(page_id) ON DELETE CASCADE
) /*$wgDBTableOptions*/;
